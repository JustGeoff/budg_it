import 'package:flutter/material.dart';

class TwoLineTitleWidget extends StatelessWidget {
  final String title;
  final String subTitle;
  final double subTitleFontSize;
  final double titleFontSize;

  TwoLineTitleWidget(
      {this.title = 'Text',
      this.subTitle,
      this.titleFontSize = 16.0,
      this.subTitleFontSize = 12.0}) {}

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontSize: titleFontSize,
          ),
        ),
        Visibility(
          visible: true,
          child: Text(
            subTitle,
            style: TextStyle(
              fontSize: subTitleFontSize,
            ),
          ),
        ),
      ],
    );
  }
}
