import 'package:flutter/material.dart';


enum ButtonType { Flat, Raised }

class ButtonWidget extends StatelessWidget {
  final String text;
  final Function click;
  final Color textColor;
  final ButtonType type;

  ButtonWidget(
      {this.text = 'Text',
      this.click,
      this.textColor = Colors.white,
      this.type = ButtonType.Raised}) {}

  @override
  Widget build(BuildContext context) {
    if (type == ButtonType.Raised) {
      return RaisedButton(
        onPressed: () {
          click();
        },
        textColor: textColor,
        child: Text(text),
      );
    } else {
      return FlatButton(
        onPressed: () {
          click();
        },
        textColor: textColor,
        child: Text(text),
      );
    }
  }
}
