import 'package:budg_it/widgets/twoLineTitle.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Colors.blueAccent,
          buttonColor: Colors.blueAccent),
      home: MyHomePage(title: 'Welcome Thabo'),
    );
  }
}

class BudgetDetail extends StatelessWidget {
  Widget _buildBudgetItem(String buttonText, Color color, bool gradient) {
    if (!gradient) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        width: 300.0,
        child: Container(
          margin: EdgeInsets.all(10),
          child: Row(
            children: [
                new Container(
                  width: 250,
                  child: new Text(
                    buttonText,
                    style: TextStyle(fontSize: 12, color: Colors.blue)
                  )
                ),
                new Align(
                  alignment: Alignment.centerRight, 
                  child: new RaisedButton(
                    onPressed: () {},
                    color: Colors.white,
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10),
                    ),
                    child: Text(
                      "Pay",
                      style: TextStyle(fontSize: 12, color: Colors.green)
                    ),
                  )
                )
            ]
          )
        ),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(10.0)
        )
      )
    ));
    } else {
      return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        width: 300.0,
        child: Container(
          margin: EdgeInsets.all(10),
          child: Row(
            children: [
                new Container(
                  width: 250,
                  child: new Text(
                    buttonText,
                    style: TextStyle(fontSize: 12, color: Colors.blue)
                  )
                ),
                new Align(
                  alignment: Alignment.centerRight, 
                  child: new RaisedButton(
                    onPressed: () {},
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10),
                    ),
                    child: Container(
                      decoration: const BoxDecoration(
                      color: Colors.blue,
                    ),
                      child: Text(
                      "Get Loan",
                      style: TextStyle(fontSize: 12, color: Colors.white)
                    ),
                    ),
                  )
                )
            ]
          )
        ),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(10.0)
        )
      )
    ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar (
        title: Text('Budget Details'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            // Navigate back to first route when tapped.
          },
          child: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              _buildBudgetItem('Car Repayment', Colors.white, false),
              _buildBudgetItem('Cellphone Payment', Colors.white, false),
              _buildBudgetItem('Rent', Colors.white, false),
              _buildBudgetItem('Need extra cash: take a loan', Colors.white, true),
              _buildBudgetItem('Internet', Colors.white, false),
              _buildBudgetItem('Student Loans', Colors.white, false),
              _buildBudgetItem('Furniture', Colors.white, false),
            ]
          ),
        ),
      )
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DecorationImage _buildDecorationImage(String image) {
    return DecorationImage(
      fit: BoxFit.fill,
      image: AssetImage(image),
    );
  }

  Widget _buildCarouselItem(Color color, String image) {
    return GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => BudgetDetail(),
            ),
          );
        },
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Container(
            width: 200.0,
            decoration: BoxDecoration(
                image: _buildDecorationImage(image),
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
          ),
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title:
            TwoLineTitleWidget(title: widget.title, subTitle: 'Find out how budget better to on WalletWize...'),
      ),
      body: Container(
        color: Colors.blueAccent,
        padding: EdgeInsets.symmetric(vertical: 15.0),
        height: 260.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            SizedBox(
              width: 10,
            ),
            _buildCarouselItem(Colors.lightBlueAccent, 'assets/mainTile.png'),
            _buildCarouselItem(Colors.indigo, 'assets/secondTile.png'),
            _buildCarouselItem(Colors.purple, 'assets/mainTile.png'),
            _buildCarouselItem(Colors.blue, 'assets/secondTile.png'),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
